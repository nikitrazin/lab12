SECTION .data
    hello: DB 'Enter line', 10
    helloLen: EQU $-hello
SECTION .bss
    buf1:RESB 80
SECTION .text
GLOBAL _start
_start:
    mov eax,4
    mov ebx,1
    mov ecx,hello
    mov edx,helloLen
int 80h
    mov eax,3
    mov ebx,0
    mov ecx,buf1
    mov edx,80
int 80h
    mov eax,4
    mov ebx,0
int 80h
