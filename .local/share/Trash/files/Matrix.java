public class Matrix
{
	private Integer [][] matrix;
	final int SIZE;
	
	public Matrix(String [] args)
	{
		SIZE = Integer.parseInt(args[0]);
        int s = Integer.parseInt(args[0]);
			if (((args.length - 1) / s) != s) 
			{
                System.out.println("Неверно задана размерность матрицы, или количество элементов");
                System.exit(-1);
            }
            int count = 1;
            matrix = new Integer[s][s];
            for (int i = 0; i < s; i++)
                for (int j = 0; j < s; j++) 
                {
                    matrix[i][j] = Integer.parseInt(args[count]);
                    count++;
				}
	};
 
  
    public Matrix(Integer[][] matrix) 
    {
        this.matrix = matrix;
        SIZE = matrix.length;
    }
  
    public void printMatrix() 
    {
      for (int i = 0; i < SIZE; i++) 
        {
            for (int j = 0; j < SIZE; j++)
                System.out.print("\t" + matrix[i][j] + "\t");
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }
 
 
    public  Integer[][] getMatrixB() 
    {
        Integer[][] matrixB = new Integer[SIZE][SIZE];
 
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++) 
            {
				 matrixB[i][j]=0;
                if(j==0)
				if(matrix[i][j] > matrix[i][j+1])
					matrixB[i][j]=1;
				if(j > 0 && j < SIZE-1)
					if(matrix[i][j] > matrix[i][j+1] && matrix[i][j] > matrix[i][j-1])
						matrixB[i][j]=1;
				if( j==SIZE-1)
					if(matrix[i][j] > matrix[i][j-1])
						matrixB[i][j]=1;
            }
        return matrixB;
    }
 
 
    public static void main(String args[]) 
    {
        Matrix matrix;
        if (args.length == 0) 
        {
            Integer	[][] matrixA = {
                    {1, 2, 1},
                    {3, 1, 2},
                    {1, 2, 2}
            };
 
            matrix = new Matrix(matrixA);
        } else 
        {
            matrix = new Matrix(args);
        }
 
        matrix.printMatrix();
 
        Integer[][] matrixB = matrix.getMatrixB();
 
        for (int i = 0; i < matrixB.length; i++) 
        {
            for (int j = 0; j < matrixB.length; j++)
                System.out.print("\t" + matrixB[i][j] + "\t");
            System.out.println();
        }
        System.out.println();
    }
 
 
}
