SECTION .data
    enter: DB 'Enter number',10
    enterLen: EQU $-enter

SECTION .bss
    buf2: RESB 80

SECTION .text
    GLOBAL _start
convert:
    xor ecx,ecx
    xor ebx,ebx
    mov eax,[buf2]
    mov bl,10
    .divide
    xor edx,edx
    div ebx
    add dl,'0'
    push dx
    inc ecx
    cmp eax,0
    jnz .divide

    .reverse
    pop ax
    mov [esi],ax
    inc esi
    loop .reverse
    ret

_start:
    mov eax,4
    mov ebx,1
    mov ecx,enter
    mov edx,enterLen

    int 80h

    mov eax,3
    mov ebx,0
    mov ecx,buf2
    mov edx,1

    int 80h

mov esi,buf2

    call convert

    mov eax,4
    mov ebx,1
    mov ecx,buf2
    mov edx,8

    int 80h
