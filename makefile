build:
	nasm -f elf64 -g -l file03.lst file03.asm
	ld -Map file03.map -o file03 file03.o
	nasm -f elf64 -g -l lab02-1.lst lab02-1.asm
	ld -Map lab02-1.map -o lab2-1 lab02-1.o
clean:
	rm -Rf file03.lst file03.map file03.o lab02-1.map lab02-1.o lab02-1.lst
